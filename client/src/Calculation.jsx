import { useState, useEffect } from "react";
import { api } from "./App";
import SquareButton from "./components/SquareButton";
import Button from "./components/Button";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Calculator = ({ handleLogout }) => {
  const [expression, setExpression] = useState("");
  const [name, setName] = useState("");
  const [result, setResult] = useState("");
  const [calculations, setCalculations] = useState([]);
  const [editingId, setEditingId] = useState("");
  const [error, setError] = useState("");

  useEffect(() => {
    const authToken = localStorage.getItem("token");
    api.defaults.headers.common["Authorization"] = authToken;
    fetchCalculations();
  }, []);

  const fetchCalculations = async () => {
    try {
      const response = await api.get("/api/calculations");
      setCalculations(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleButtonClick = (value) => {
    setExpression(expression + value);
  };

  const calculateResult = () => {
    try {
      // Evaluating the expression using eval()
      const calculatedResult = eval(expression);
      setResult(calculatedResult);
    } catch (error) {
      console.log(error);
    }
  };

  const handleCalculate = () => {
    calculateResult();
  };

  const handleSave = async () => {
    if (!name) return toast.error("Please enter name");
    if (!expression) return toast.error("Please enter expression");
    if (!result) return toast.error("Please calculate");

    try {
      if (editingId) {
        await api.put(`/api/calculations/${editingId}`, {
          name,
          expression,
          result: parseFloat(result),
        });
        setEditingId("");
      } else {
        await api.post("/api/calculations", {
          name,
          expression,
          result: parseFloat(result),
        });
      }
      fetchCalculations();
      setExpression("");
      setName("");
      setResult("");
      setError("");
    } catch (error) {
      console.log(error);
    }
  };

  const handleEdit = (id, expression, name, result) => {
    setEditingId(id);
    setExpression(expression);
    setName(name);
    setResult(result);
    setError("");
  };

  const handleDelete = async (id) => {
    try {
      await api.delete(`/api/calculations/${id}`);
      fetchCalculations();
    } catch (error) {
      console.log(error);
    }
  };

  const handleClear = () => {
    setExpression("");
    setName("");
    setResult("");
    setEditingId("");
    setError("");
  };

  const handleKeyDown = (e) => {
    const validChars = "0123456789+-*/";
    const keyPressed = e.key;

    if (
      !validChars.includes(keyPressed) &&
      keyPressed !== "Enter" &&
      keyPressed !== "Backspace"
    ) {
      e.preventDefault();
    }
  };

  return (
    <div className="max-w-3xl p-6 bg-white rounded shadow mx-auto">
      <h2 className="text-2xl font-bold mb-4">Calculator</h2>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-4 mb-4">
        <div className="bg-blue-300 bg-opacity-20 h-fit p-2 rounded-md ">
          <input
            type="text"
            value={expression}
            onKeyDown={handleKeyDown}
            onChange={(e) => setExpression(e.target.value)}
            className="w-full p-2 border rounded mb-2"
            required
          />

          <input
            type="text"
            value={result}
            onChange={(e) => setResult(e.target.value)}
            placeholder="Result"
            className="w-full p-2 border rounded mb-2"
            required
          />

          <div className="grid grid-cols-4 gap-1 mb-4">
            <SquareButton onClick={() => handleButtonClick("1")}>
              1
            </SquareButton>
            <SquareButton onClick={() => handleButtonClick("2")}>
              2
            </SquareButton>
            <SquareButton onClick={() => handleButtonClick("3")}>
              3
            </SquareButton>
            <SquareButton onClick={() => handleButtonClick("+")}>
              +
            </SquareButton>

            <SquareButton onClick={() => handleButtonClick("4")}>
              4
            </SquareButton>
            <SquareButton onClick={() => handleButtonClick("5")}>
              5
            </SquareButton>
            <SquareButton onClick={() => handleButtonClick("6")}>
              6
            </SquareButton>
            <SquareButton onClick={() => handleButtonClick("-")}>
              -
            </SquareButton>

            <SquareButton onClick={() => handleButtonClick("7")}>
              7
            </SquareButton>
            <SquareButton onClick={() => handleButtonClick("8")}>
              8
            </SquareButton>
            <SquareButton onClick={() => handleButtonClick("9")}>
              9
            </SquareButton>
            <SquareButton onClick={() => handleButtonClick("*")}>
              *
            </SquareButton>

            <SquareButton onClick={() => handleButtonClick("0")}>
              0
            </SquareButton>
            <SquareButton onClick={() => handleButtonClick("/")}>
              /
            </SquareButton>
          </div>
          <Button onClick={handleClear} variant="danger">
            Clear
          </Button>
          <Button onClick={handleCalculate} variant="primary">
            Calculate
          </Button>
        </div>

        <div>
          <input
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            placeholder="Enter Name"
            className="w-full p-2 border rounded mb-2"
            required
          />

          <Button onClick={handleSave} variant="success">
            {editingId ? "Update" : "Save"}
          </Button>

          <Button variant="danger" onClick={handleLogout}>
            Log Out
          </Button>

          {error && <p className="text-red-500 mt-4">{error}</p>}

          <h3 className="text-lg font-bold mt-8">Calculations</h3>
          <ul className="mt-2">
            {calculations.map((calc) => (
              <li
                key={calc._id}
                className="flex items-center justify-between mb-2 border shadow-md p-1 rounded-md"
              >
                <span className="font-semibold">{calc.name}</span>
                <div>
                  <Button
                    variant="success"
                    onClick={() =>
                      handleEdit(
                        calc._id,
                        calc.expression,
                        calc.name,
                        calc.result
                      )
                    }
                    className="mr-2"
                  >
                    Edit
                  </Button>
                  <Button
                    variant="danger"
                    onClick={() => handleDelete(calc._id)}
                  >
                    Delete
                  </Button>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Calculator;
