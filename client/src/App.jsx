import { useEffect, useState } from "react";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Calculator from "./Calculation";
import Button from "./components/Button";

export const api = axios.create({
  baseURL: "http://localhost:5000",
});

const App = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const authToken = localStorage.getItem("token");
    if (authToken) setIsLoggedIn(true);
  }, []);

  const handleRegister = async () => {
    if (!email.includes("@")) return toast.error("Invalid Email");
    if (password.length < 8) return toast.error("Invalid Password");
    try {
      const response = await api.post("/api/register", { email, password });
      console.log(response.data);
      setEmail("");
      setPassword("");
      toast.success("Registration successful!");
    } catch (error) {
      console.log(error);
      toast.error(error.response.data.message);
    }
  };

  const handleLogin = async () => {
    if (!email.includes("@")) return toast.error("Invalid Email");
    if (password.length < 8) return toast.error("Invalid Password");

    try {
      const response = await api.post("/api/login", { email, password });
      console.log(response.data);
      localStorage.setItem("token", response.data.token);
      setIsLoggedIn(true);
      setEmail("");
      setPassword("");
      toast.success("Login successful!");
    } catch (error) {
      console.log(error);
      toast.error("Login failed!");
    }
  };

  const handleLogout = () => {
    setIsLoggedIn(false);
    localStorage.removeItem("token");
  };

  if (!isLoggedIn) {
    return (
      <div className="min-h-screen bg-blue-400">
        <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8 ">
          <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm bg-white bg-opacity-80 p-8 rounded-md">
            <h1 className="text-4xl mb-4">Login/Register</h1>

            <div>
              <label
                htmlFor="email"
                className="block text-sm font-medium leading-6 text-gray-900"
              >
                Email address
              </label>
              <div className="mt-2">
                <input
                  type="text"
                  value={email}
                  placeholder="Email"
                  onChange={(e) => setEmail(e.target.value)}
                  className="w-full p-2 border rounded mb-2"
                />
              </div>
            </div>

            <div>
              <div className="flex items-center justify-between">
                <label
                  htmlFor="password"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Password
                </label>
              </div>
              <div className="mt-2">
                <input
                  type="password"
                  value={password}
                  placeholder="Password"
                  onChange={(e) => setPassword(e.target.value)}
                  className="w-full p-2 border rounded mb-2"
                />
              </div>
            </div>

            <div>
              <Button variant="primary" onClick={handleRegister}>
                Register
              </Button>
              <Button variant="success" onClick={handleLogin}>
                Login
              </Button>
              <ToastContainer />
            </div>
          </div>
        </div>
      </div>
    );
  }

  return (
    <div className="min-h-screen bg-blue-400 p-10">
      <ToastContainer />
      <Calculator handleLogout={handleLogout} />
    </div>
  );
};

export default App;
