import React from "react";

const SquareButton = ({ children, onClick }) => {
  return (
    <button
      className="w-full h-10 m-0.5 bg-blue-500 hover:bg-blue-600 text-white rounded-md"
      onClick={onClick}
    >
      {children}
    </button>
  );
};

export default SquareButton;
