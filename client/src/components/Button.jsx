const Button = ({ onClick, variant, children }) => {
  // Define the class based on the color variant
  let className = "px-4 py-2 m-1 rounded text-white";
  switch (variant) {
    case "primary":
      className += " bg-blue-500 hover:bg-blue-600";
      break;
    case "secondary":
      className += " bg-gray-500 hover:bg-gray-600";
      break;
    case "success":
      className += " bg-green-500 hover:bg-green-600";
      break;
    case "danger":
      className += " bg-red-500 hover:bg-red-600";
      break;
    default:
      className += " bg-gray-500 hover:bg-gray-600";
  }

  return (
    <button className={className} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
