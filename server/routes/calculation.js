const express = require("express");
const Calculation = require("../models/Calculation");
const authenticate = require("../middleware/authenticate");

const router = express.Router();

// Get all calculations for a user
router.get("/", authenticate, async (req, res) => {
  try {
    const calculations = await Calculation.find({ user: req.userId });
    res.json(calculations);
  } catch (error) {
    res.status(500).json({ message: "Failed to fetch calculations" });
  }
});

// Create a new calculation
router.post("/", authenticate, async (req, res) => {
  try {
    const { name, expression, result } = req.body;
    const calculation = new Calculation({
      name,
      expression,
      result,
      user: req.userId,
    });
    await calculation.save();
    res.status(201).json({ message: "Calculation saved successfully" });
  } catch (error) {
    res.status(500).json({ message: "Failed to save calculation" });
  }
});

// Update a calculation
router.put("/:id", authenticate, async (req, res) => {
  try {
    const { name, expression, result } = req.body;
    const calculation = await Calculation.findOneAndUpdate(
      { _id: req.params.id, user: req.userId },
      { name, expression, result },
      { new: true }
    );
    if (!calculation) {
      return res.status(404).json({ message: "Calculation not found" });
    }
    res.json({ message: "Calculation updated successfully" });
  } catch (error) {
    res.status(500).json({ message: "Failed to update calculation" });
  }
});

// Delete a calculation
router.delete("/:id", authenticate, async (req, res) => {
  try {
    const calculation = await Calculation.findOneAndDelete({
      _id: req.params.id,
      user: req.userId,
    });
    if (!calculation) {
      return res.status(404).json({ message: "Calculation not found" });
    }
    res.json({ message: "Calculation deleted successfully" });
  } catch (error) {
    res.status(500).json({ message: "Failed to delete calculation" });
  }
});

module.exports = router;
