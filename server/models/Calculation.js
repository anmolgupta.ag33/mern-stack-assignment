const mongoose = require("mongoose");

const CalculationSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  expression: {
    type: String,
    required: true,
  },
  result: {
    type: Number,
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
});

const Calculation = mongoose.model("Calculation", CalculationSchema);

module.exports = Calculation;
